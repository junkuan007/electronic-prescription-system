/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

/**
 *
 * @author junku
 */
public class Account {

    int id;
    String accountType;

    public Account(int id, String accountType) {
        this.id = id;
        this.accountType = accountType;
    }

    public Account(String accountType) {
        this.accountType = accountType;
    }

    public Account() {
    }

    public int getId() {
        return id;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
    
}