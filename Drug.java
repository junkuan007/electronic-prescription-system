/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

/**
 *
 * @author junku
 */
public class Drug {

    int drugId;
    String drugName;
    String description;

    public Drug() {
    }

    public Drug(int drugId, String drugName, String description) {
        this.drugId = drugId;
        this.drugName = drugName;
        this.description = description;
    }

    public Drug(String drugName, String description) {
        this.drugName = drugName;
        this.description = description;
    }
    

    public int getDrugId() {
        return drugId;
    }

    public String getDrugName() {
        return drugName;
    }

    public String getDescription() {
        return description;
    }

    public void setDrugId(int drugId) {
        this.drugId = drugId;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Drug{" + "drugId=" + drugId + ", drugName=" + drugName + ", description=" + description + '}';
    }
}
