/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

/**
 *
 * @author junku
 */
public class Prescription {

    int tokenId;
    int userId;
    String firstName;
    String lastName;
    String email;
    String drugName;
    String dosage;
    String dateOfPrescription;
    String status;
    String dateOfCollection;

    public Prescription() {
    }

    public Prescription(int tokenId, int userId, String firstName, String lastName, String email, String drugName, String dosage, String dateOfPrescription, String status, String dateOfCollection) {
        this.tokenId = tokenId;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.drugName = drugName;
        this.dosage = dosage;
        this.dateOfPrescription = dateOfPrescription;
        this.status = status;
        this.dateOfCollection = dateOfCollection;
    }

    public Prescription(int userId, String firstName, String lastName, String email, String drugName, String dosage, String dateOfPrescription, String status, String dateOfCollection) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.drugName = drugName;
        this.dosage = dosage;
        this.dateOfPrescription = dateOfPrescription;
        this.status = status;
        this.dateOfCollection = dateOfCollection;
    }

    public Prescription(int userId, String firstName, String lastName, String email, String drugName, String dosage, String dateOfPrescription, String status) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.drugName = drugName;
        this.dosage = dosage;
        this.dateOfPrescription = dateOfPrescription;
        this.status = status;
    }

    public Prescription(int tokenId) {
        this.tokenId = tokenId;
    }
    
    public int getTokenId() {
        return tokenId;
    }

    public int getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getDrugName() {
        return drugName;
    }

    public String getDosage() {
        return dosage;
    }

    public String getDateOfPrescription() {
        return dateOfPrescription;
    }

    public String getStatus() {
        return status;
    }

    public String getDateOfCollection() {
        return dateOfCollection;
    }

    public void setTokenId(int tokenId) {
        this.tokenId = tokenId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public void setDateOfPrescription(String dateOfPrescription) {
        this.dateOfPrescription = dateOfPrescription;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDateOfCollection(String dateOfCollection) {
        this.dateOfCollection = dateOfCollection;
    }

    @Override
    public String toString() {
        return "Prescription{" + "tokenId=" + tokenId + ", userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", drugName=" + drugName + ", dosage=" + dosage + ", dateOfPrescription=" + dateOfPrescription + ", status=" + status + ", dateOfCollection=" + dateOfCollection + '}';
    }
}
