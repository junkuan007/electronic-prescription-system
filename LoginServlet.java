/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author junku
 */
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String accountType = request.getParameter("accountType");
            String email = request.getParameter("email");
            String password = request.getParameter("password");

            UserDatabase dB = new UserDatabase(MySQLConnection.getCon());
            User user = dB.logUser(accountType, email, password);

            if (user != null) {
                if (accountType.equals("ADMIN")) {
                    HttpSession session = request.getSession();
                    session.setAttribute("logUser", user);
                    response.sendRedirect("adminMainPage.jsp");
                    
                } else if (accountType.equals("PHARMACIST")) {
                    HttpSession session = request.getSession();
                    session.setAttribute("logUser", user);
                    response.sendRedirect("pharmacistMainPage.jsp");
                    
                } else if (accountType.equals("DOCTOR")) {
                    HttpSession session = request.getSession();
                    session.setAttribute("logUser", user);
                    response.sendRedirect("doctorMainPage.jsp");
                    
                } else if (accountType.equals("PATIENT")) {
                    HttpSession session = request.getSession();
                    session.setAttribute("logUser", user);
                    response.sendRedirect("patientMainPage.jsp");
                }
            } else {
                request.setAttribute("error", "Incorrect email or password");
                RequestDispatcher rD = request.getRequestDispatcher("index.jsp");
                rD.include(request, response);
            }
        }
    }

    /*
                if (accountType.equals("ADMIN")) {
                    HttpSession session = request.getSession();
                    session.setAttribute("logUser", user);
                    response.sendRedirect("adminMainPage.jsp");
                } else if (accountType.equals("PHARMACSIT")) {
                    HttpSession session = request.getSession();
                    session.setAttribute("logUser", user);
                    response.sendRedirect("pharmacistMainPage.jsp");
                } else if (accountType.equals("DOCTOR")) {
                    HttpSession session = request.getSession();
                    session.setAttribute("logUser", user);
                    response.sendRedirect("doctorMainPage.jsp");
                } else if (accountType.equals("PATIENT")) {
                    HttpSession session = request.getSession();
                    session.setAttribute("logUser", user);
                    response.sendRedirect("patientMainPage.jsp");
                } */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
