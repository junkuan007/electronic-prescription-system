/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author junku
 */
public class PrescriptionDatabase {

    Connection con;

    public PrescriptionDatabase(Connection con) {
        this.con = con;
    }

    //add prescription information to database 
    public boolean addPrescription(Prescription prescription) {
        boolean test = false;

        try {
            String query = "insert into prescriptions (userId,firstName,lastName,email,drugName,dosage,dateOfPrescription,status) values(?,?,?,?,?,?,?,?)";
            PreparedStatement pst = this.con.prepareStatement(query);
            pst.setInt(1, prescription.getUserId());
            pst.setString(2, prescription.getFirstName());
            pst.setString(3, prescription.getLastName());
            pst.setString(4, prescription.getEmail());
            pst.setString(5, prescription.getDrugName());
            pst.setString(6, prescription.getDosage());
            pst.setString(7, prescription.getStatus());
            pst.setString(8, prescription.getDateOfPrescription());
            pst.executeUpdate();
            test = true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return test;
    }

    //Patient Main Page list
    public List<Prescription> getPatientPrescription(String input) {
        List<Prescription> prescription = new ArrayList<>();
        try {
            String query = "select * from prescriptions where userId like '" + input + "'";
            Statement statement = this.con.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                int tokenId = rs.getInt("tokenId");
                int userId = rs.getInt("userId");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String email = rs.getString("email");
                String drugName = rs.getString("drugName");
                String dosage = rs.getString("dosage");
                String dateOfPrescription = rs.getString("dateOfPrescription");
                String status = rs.getString("status");
                String dateOfCollection = rs.getString("dateOfCollection");

                Prescription row = new Prescription(tokenId, userId, firstName, lastName, email, drugName, dosage, dateOfPrescription, status, dateOfCollection);
                prescription.add(row);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return prescription;
    }

    //Patient Filter search
    public List<Prescription> getPatientFilteredPrescription(String id, String input) {
        List<Prescription> prescription = new ArrayList<>();
        try {
            String query = "select * from prescriptions where userId like '" + id + "' and tokenId like '" + input + "' ";
            Statement statement = this.con.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                int tokenId = rs.getInt("tokenId");
                int userId = rs.getInt("userId");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String email = rs.getString("email");
                String drugName = rs.getString("drugName");
                String dosage = rs.getString("dosage");
                String dateOfPrescription = rs.getString("dateOfPrescription");
                String status = rs.getString("status");
                String dateOfCollection = rs.getString("dateOfCollection");

                Prescription row = new Prescription(tokenId, userId, firstName, lastName, email, drugName, dosage, dateOfPrescription, status, dateOfCollection);
                prescription.add(row);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return prescription;
    }

    public List<Prescription> getFilteredPrescription(String input) {
        List<Prescription> prescription = new ArrayList<>();
        try {
            String query = "select * from prescriptions where tokenId like '" + input + "' or userId like '" + input + "' or status like '" + input + "'";
            Statement statement = this.con.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                int tokenId = rs.getInt("tokenId");
                int userId = rs.getInt("userId");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String email = rs.getString("email");
                String drugName = rs.getString("drugName");
                String dosage = rs.getString("dosage");
                String dateOfPrescription = rs.getString("dateOfPrescription");
                String status = rs.getString("status");
                String dateOfCollection = rs.getString("dateOfCollection");

                Prescription row = new Prescription(tokenId, userId, firstName, lastName, email, drugName, dosage, dateOfPrescription, status, dateOfCollection);
                prescription.add(row);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return prescription;
    }

    //List all prescriptions
    public List<Prescription> getAllPrescriptions() {
        List<Prescription> prescription = new ArrayList<>();
        try {

            String query = "select * from prescriptions order by tokenId desc";
            PreparedStatement pt = this.con.prepareStatement(query);
            ResultSet rs = pt.executeQuery();

            while (rs.next()) {
                int tokenId = rs.getInt("tokenId");
                int userId = rs.getInt("userId");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String email = rs.getString("email");
                String drugName = rs.getString("drugName");
                String dosage = rs.getString("dosage");
                String dateOfPrescription = rs.getString("dateOfPrescription");
                String status = rs.getString("status");
                String dateOfCollection = rs.getString("dateOfCollection");

                Prescription row = new Prescription(tokenId, userId, firstName, lastName, email, drugName, dosage, dateOfPrescription, status, dateOfCollection);
                prescription.add(row);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return prescription;
    }

    //get single prescription information in update page
    public Prescription getSinglePrescription(int tokenId) {
        Prescription p = null;

        try {
            String query = "select * from prescriptions where tokenId=? ";

            PreparedStatement pt = this.con.prepareStatement(query);
            pt.setInt(1, tokenId);
            ResultSet rs = pt.executeQuery();

            while (rs.next()) {
                int bid = rs.getInt("tokenId");
                int userId = rs.getInt("userId");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String email = rs.getString("email");
                String drugName = rs.getString("drugName");
                String dosage = rs.getString("dosage");
                String dateOfPrescription = rs.getString("dateOfPrescription");
                String status = rs.getString("status");
                String dateOfCollection = rs.getString("dateOfCollection");

                p = new Prescription(bid, userId, firstName, lastName, email, drugName, dosage, dateOfPrescription, status, dateOfCollection);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return p;
    }

    //edit prescription information
    public boolean editPrescriptionInformation(Prescription prescription) {
        boolean test = false;
        try {
            String query = "update prescriptions set userId=?, firstName=?, lastName=?, email=?, drugName=?, dosage=?, dateOfPrescription=?, status=?, dateOfCollection=? where tokenId=?";
            PreparedStatement pt = this.con.prepareStatement(query);

            pt.setInt(1, prescription.getUserId());
            pt.setString(2, prescription.getFirstName());
            pt.setString(3, prescription.getLastName());
            pt.setString(4, prescription.getEmail());
            pt.setString(5, prescription.getDrugName());
            pt.setString(6, prescription.getDosage());
            pt.setString(7, prescription.getDateOfPrescription());
            pt.setString(8, prescription.getStatus());
            pt.setString(9, prescription.getDateOfCollection());
            pt.setInt(10, prescription.getTokenId());
            pt.executeUpdate();

            test = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return test;
    }

    //Extract out Token ID
    public Prescription retrieveTokenID() {
        Prescription p = null;
        try {

            String query = "select tokenId from prescriptions order by tokenId desc limit 1";
            PreparedStatement pt = this.con.prepareStatement(query);
            ResultSet rs = pt.executeQuery();

            while (rs.next()) {
                int tokenId = rs.getInt("tokenId");
                
                p = new Prescription(tokenId);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return p;
    }
}
