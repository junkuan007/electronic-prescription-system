/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.ResultSet;
/**
 *
 * @author junku
 */
public class UserDatabase {

    Connection con;

    public UserDatabase(Connection con) {
        this.con = con;
    }

    //add user information to database
    public boolean addUser(User user) {
        boolean test = false;

        try {
            String query = "insert into users (accountType,firstName,lastName,email,password) values(?,?,?,?,?)";
            PreparedStatement pst = this.con.prepareStatement(query);
            pst.setString(1, user.getAccountType());
            pst.setString(2, user.getFirstName());
            pst.setString(3, user.getLastName());
            pst.setString(4, user.getEmail());
            pst.setString(5, user.getPassword());
            pst.executeUpdate();
            test = true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return test;
    }

//    retrieve the user details from databse
    public List<User> getAllUsers() {
        List<User> user = new ArrayList<>();

        try {

            String query = "select * from users order by id desc";
            PreparedStatement pt = this.con.prepareStatement(query);
            ResultSet rs = pt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String accountType = rs.getString("accountType");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String email = rs.getString("email");
                String password = rs.getString("password");

                User row = new User(id, accountType, firstName, lastName, email, password);
                user.add(row);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    //List filtered users
    public List<User> getFilteredUsers(String input) {
        List<User> user = new ArrayList<>();
        
        try {
            
            String query = "select * from users where id like '"+input+"' or accountType like '"+input+"' or email like '"+input+"' ";
            PreparedStatement pt = this.con.prepareStatement(query);
            ResultSet rs = pt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String accountType = rs.getString("accountType");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String email = rs.getString("email");
                String password = rs.getString("password");

                User row = new User(id, accountType, firstName, lastName, email, password);
                user.add(row);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    //    get single user information in update page
    public User getSingleUser(int id) {
        User u = null;

        try {
            String query = "select * from users where id=? ";

            PreparedStatement pt = this.con.prepareStatement(query);
            pt.setInt(1, id);
            ResultSet rs = pt.executeQuery();

            while (rs.next()) {
                int bid = rs.getInt("id");
                String accountType = rs.getString("accountType");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String email = rs.getString("email");
                String password = rs.getString("password");
                u = new User(bid, accountType, firstName, lastName, email, password);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return u;
    }

    //    edit user information
    public boolean editUserInformation(User user) {
        boolean test = false;
        try {
            String query = "update users set accountType=?, firstName=?, lastName=?, email=? where id=?";
            PreparedStatement pt = this.con.prepareStatement(query);
            pt.setString(1, user.getAccountType());
            pt.setString(2, user.getFirstName());
            pt.setString(3, user.getLastName());
            pt.setString(4, user.getEmail());
            pt.setInt(5, user.getId());
            pt.executeUpdate();

            test = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return test;
    }

    //User login
    public User logUser(String accountType, String email, String password) {
        User user = null;
        try {

            String query = "select * from users where accountType=? and email=? and password=?";
            PreparedStatement pStatement = this.con.prepareStatement(query);
            pStatement.setString(1, accountType);
            pStatement.setString(2, email);
            pStatement.setString(3, password);

            ResultSet rSet = pStatement.executeQuery();

            if (rSet.next()) {
                user = new User();
                user.setId(rSet.getInt("id"));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }
}
