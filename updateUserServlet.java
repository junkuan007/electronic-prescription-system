/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author junku
 */
public class updateUserServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            //Fetch data from the update form
            int id = Integer.parseInt(request.getParameter("id"));
            String accountType = request.getParameter("accountType");
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String email = request.getParameter("email");

            User user = new User(accountType, firstName, lastName, email);
            user.setAccountType(accountType);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            user.setId(id);

            try {
                UserDatabase userDatabase = new UserDatabase(MySQLConnection.getCon());
                boolean success = userDatabase.editUserInformation(user);
                if (success) {
                    out.println("Successful");
                    response.sendRedirect("adminMainPage.jsp");
                } else if (accountType == null) {
                    request.setAttribute("error", "Please select Account Type.");
                    RequestDispatcher rD = request.getRequestDispatcher("viewAndUpdateUser.jsp");
                    rD.include(request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try (PrintWriter out = response.getWriter()) {

            try {
                AccountDatabase accountDatabase = new AccountDatabase(MySQLConnection.getCon());

                String op = request.getParameter("operation");

                if (op.equals("accType")) {
                    List<Account> list = accountDatabase.getAllAccounts();
                    Gson json = new Gson();
                    String accountList = json.toJson(list);
                    response.setContentType("text/html");
                    response.getWriter().write(accountList);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
