/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 *
 * @author junku
 */
public class TestRunner {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(LoginTest.class);
        for(Failure failure : result.getFailures()) {
            System.out.println("Succuessful test: " + failure.toString());
        }
        System.out.println("Succussful test: " + result.wasSuccessful());
    }
}
