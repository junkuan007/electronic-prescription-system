/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author junku
 */
public class AccountDatabase {

    Connection con;
    PreparedStatement pst;
    String query;
    ResultSet rS;

    public AccountDatabase(Connection con) {
        this.con = con;
    }

    //add account information to database
    public boolean addAccount(Account account) {
        boolean test = false;

        try {
            String query = "insert into accounts (accountType) values(?)";
            pst = this.con.prepareStatement(query);
            pst.setString(1, account.getAccountType());
            pst.executeUpdate();
            test = true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return test;
    }
    //List all accounts
    public List<Account> getAllAccounts() {
        List<Account> account = new ArrayList<>();
        try {
            query = "select * from accounts";
            pst = this.con.prepareStatement(query);
            rS = pst.executeQuery();
            while (rS.next()) {
                int id = rS.getInt("id");
                String accountType = rS.getString("accountType");
                
                Account row = new Account(id, accountType);
                account.add(row);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return account;
    }
    
    //List filtered accounts
    public List<Account> getFilteredAccounts(String input) {
        List<Account> account = new ArrayList<>();
        
        try {
            
            String query = "select * from accounts where id like '"+input+"' or accountType like '"+input+"'";
            PreparedStatement pt = this.con.prepareStatement(query);
            ResultSet rs = pt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String accountType = rs.getString("accountType");

                Account row = new Account(id, accountType);
                account.add(row);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return account;
    }
    
    //get single account
    public Account getSingleAccount(int id) {
        Account a = null;

        try {
            String query = "select * from accounts where id=? ";

            PreparedStatement pt = this.con.prepareStatement(query);
            pt.setInt(1, id);
            ResultSet rs = pt.executeQuery();

            while (rs.next()) {
                int bid = rs.getInt("id");
                String accountType = rs.getString("accountType");
                a = new Account(bid, accountType);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return a;
    }
    
    //    edit Account Type
    public boolean editAccountType(Account account) {
        boolean test = false;
        try {
            String query = "update accounts set accountType=? where id=?";
            PreparedStatement pt = this.con.prepareStatement(query);
            pt.setString(1, account.getAccountType());
            pt.setInt(2, account.getId());
            pt.executeUpdate();

            test = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return test;
    }
}
