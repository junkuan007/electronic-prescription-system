/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author junku
 */
public class updatePrescriptionServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            //Fetch data from the update form
            int tokenId = Integer.parseInt(request.getParameter("tokenId"));
            int userId = Integer.parseInt(request.getParameter("userId"));
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String email = request.getParameter("email");
            String drugName = request.getParameter("drugName");
            String dosage = request.getParameter("dosage");
            String dateOfPrescription = request.getParameter("dateOfPrescription");
            String status = request.getParameter("status");
            String dateOfCollection = request.getParameter("dateOfCollection");

            Prescription prescription = new Prescription(userId, firstName, lastName, email, drugName, dosage, dateOfPrescription, status, dateOfCollection);
            prescription.setUserId(userId);
            prescription.setFirstName(firstName);
            prescription.setLastName(lastName);
            prescription.setEmail(email);
            prescription.setDrugName(drugName);
            prescription.setDosage(dosage);
            prescription.setDateOfPrescription(dateOfPrescription);
            prescription.setTokenId(tokenId);

            try {
                PrescriptionDatabase prescriptionDatabase = new PrescriptionDatabase(MySQLConnection.getCon());
                boolean success = prescriptionDatabase.editPrescriptionInformation(prescription);
                if (success) {
                    System.out.println("Prescription created.");
                    response.sendRedirect("doctorMainPage.jsp");
                } 
                else if (drugName == null) {
                    System.out.println("1.");
                    request.setAttribute("errorDrug", "Please select Drug.");
                    RequestDispatcher rD = request.getRequestDispatcher("updatePrescriptionDoctor.jsp");
                    rD.include(request, response);
                } 
                else if (dosage == null) {
                    request.setAttribute("errorDosage", "Please select Dosage.");
                    RequestDispatcher rD = request.getRequestDispatcher("updatePrescriptionDoctor.jsp");
                    rD.include(request, response);
                } 
                else if (status == null) {
                    request.setAttribute("errorStatus", "Please select Status.");
                    RequestDispatcher rD = request.getRequestDispatcher("updatePrescriptionDoctor.jsp");
                    rD.include(request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
