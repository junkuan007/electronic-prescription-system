/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author junku
 */
public class DrugDatabase {

    Connection con;

    public DrugDatabase(Connection con) {
        this.con = con;
    }

    //retrieve the drug details from databse
    public List<Drug> getAllDrugs() {
        List<Drug> drug = new ArrayList<>();

        try {

            String query = "select * from drugs";
            PreparedStatement pt = this.con.prepareStatement(query);
            ResultSet rs = pt.executeQuery();

            while (rs.next()) {
                int drugId = rs.getInt("drugId");
                String drugName = rs.getString("drugName");
                String description = rs.getString("description");

                Drug row = new Drug(drugId, drugName, description);
                drug.add(row);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return drug;
    }
}
