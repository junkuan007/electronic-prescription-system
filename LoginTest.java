package newpackage;

/**
 *
 * @author junku
 */
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.junit.Assert.*;


public class LoginTest {

    private LoginServlet tester;
    private HttpServletRequest request;
    private HttpServletResponse response;

    public void setUp() throws Exception {
        this.tester = new LoginServlet();
        request = new HttpServletRequest();
        request = new HttpServletResponse();
        
    }

    public void tearDown() throws Exception {
        tester = null;
    }

    public void testUser() throws ServletException, IOException {
        request.addParameter("accountType", "PATIENT");
        request.addParameter("email", "jklee007@mymail.sim.edu.sg");
        request.addParameter("password", "12345");

        tester.processRequest(request, response);
        assertTrue("Enter correct information.", response.getContentType());
    }
}
